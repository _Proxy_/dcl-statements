create user rentaluser with password 'rentalpassword';
grant select on customer to rentaluser;

set role rentaluser;
select * from customer;

set role to postgres;

create group rental;
grant insert, update on public.rental to rental;
grant select on public.rental to rental;
grant select on public.customer to rental;
grant usage, select on sequence rental_rental_id_seq to rental;
alter group rental add user rentaluser;

set role rentaluser;
insert into rental ( rental_date,inventory_id,customer_id,return_date,staff_id) 
values (CURRENT_TIMESTAMP,(select max(inventory_id) from rental),(select max(customer_id) from rental),CURRENT_TIMESTAMP + INTERVAL '2 days',1);
update rental as r
set return_date = '2007-05-25 00:54:33.000 +0500'
where r.rental_id ='573';

set role postgres;
revoke insert on public.rental from rental;
set role rentaluser;
insert into rental ( rental_date,inventory_id,customer_id,return_date,staff_id) 
values (CURRENT_TIMESTAMP,(select max(inventory_id) from rental),(select max(customer_id) from rental),CURRENT_TIMESTAMP + INTERVAL '2 days',1);

set role postgres;
create role client_Mary_Smith;
grant select on rental  to client_Mary_Smith;
grant select on payment  to client_Mary_Smith;

create policy allowed_Mary_Smith_payment
on payment 
for select
to client_Mary_Smith
using(customer_id='1' and customer_id is not null );

create policy allowed_Mary_Smith_rental
on rental
for select
to client_Mary_Smith
using (customer_id = '1' and customer_id is not null ); 

--NOTE: after compiling code below the INSERT and UPDATE may not work so code(insert, update) which located on the top actually works well
alter table rental enable row level security;
alter table payment enable row level security;

set role client_Mary_Smith;

select*
from rental;

select *
from payment;

set role postgres;

alter table rental disable row level security;
alter table payment disable row level security;